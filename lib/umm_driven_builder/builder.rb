class UMMDrivenBuilder
  def initialize(project)
    @project          = project
    @ordered_elements = []
    @registry         = {}
  end
  
  def register(element)
    @registry[element.class] ||= []
    @registry[element.class] << element
  end
  
  def registry_entries(type)
    @registry[type] || []
  end
  
  # No, it isn't efficient...
  def order_packages
    done = []
    pkgs = (registry_entries(UmlMetamodel::Profile) + registry_entries(UmlMetamodel::Model) + registry_entries(UmlMetamodel::Package))
    while done.count < pkgs.count do
      pkgs.each { |pkg| order_package(pkg, done) }
    end
    @ordered_elements += done
  end
  
  def order_package(pkg, done)
    if pkg.owner.nil? || done.include?(pkg.owner)
      done << pkg unless done.include?(pkg)
      pkg.packages.each { |pkg| order_package(pkg, done) }
    end
  end 
  
  # No support for inner classes?
  def order_classifiers
    types = [UmlMetamodel::Primitive, UmlMetamodel::Enumeration, UmlMetamodel::Interface, UmlMetamodel::Datatype, UmlMetamodel::Class]
    unordered_classifiers = types.collect { |t| registry_entries(t) }.flatten
    unordered_classifiers.sort! do |a,b|
      case
      when a.primitive? && !b.primitive?; -1
      when b.primitive? && !a.primitive?;  1
      when a.complex_attribute? && !b.complex_attribute?; -1
      when b.complex_attribute? && !a.complex_attribute?;  1
      when a.interface? && !b.interface?; -1
      when b.interface? && !a.interface?;  1
      else; 0
      end
    end
    done = []
    last_count = done.count
    while last_count < unordered_classifiers.count do
      unordered_classifiers.each do |cls|
        predecessors  = cls.parents
        predecessors += cls.implements if cls.class?
        unprocessed_predecessors = (predecessors - done - UmlMetamodel.primitives)
        next if unprocessed_predecessors.any?
        attr_types = cls.properties.select { |prop| !prop.association }.collect(&:type)
        nonready_attr_types = (attr_types - done - UmlMetamodel.primitives)
        if nonready_attr_types.empty?
          done << cls unless done.include?(cls)
        end
      end
      unless done.count > last_count
        raise "Failed to order classifiers.  The problem is among the following classes: #{(done - unordered_classifiers).inspect}"
      end
      last_count = done.count
    end
    @ordered_elements += done
    # @ordered_elements.sort! do |a,b|
    # end
  end

  def skip
    ""
  end

  def indentation
    @indentation
  end

  def indent
    @indentation += 2
  end

  def unindent
    @indentation -= 2
    @indentation = 0 if @indentation < 0
  end
  
  def _write(var, strings)
    Array(strings).each { |string| var << (" " * @indentation + string) }
  end

  def model_name
    Appellation.model_name(@project)
  end
  def generated_gem_name
    Appellation.gem_name_generated(@project)
  end
  def model_name_generated
    Appellation.model_name_generated(@project)
  end
  def version
    @project.version
  end
end