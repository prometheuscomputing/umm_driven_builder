module PrometheusFiles
  PROMETHEUS_DIR = ENV['PROMETHEUS'] || File.join(ENV['HOME'], 'Prometheus')
  def self.root_dir(testing = false)
    $generator_test_dir
    dir = $generator_test_dir || File.join(PROMETHEUS_DIR, 'Generated_Code')
    FileUtils.mkdir_p(dir) unless File.exists?(dir)
    raise "#{dir} already exists, but is not a directory" unless File.directory?(dir)
    dir
  end

  def self.save_to_file(filename, output, subdirs = [], executable = false)
    starting_directory = Dir.pwd
    Dir.chdir(root_dir)
    subdirs.each do |dir|
      FileUtils.mkdir_p(dir) unless File.exists?(dir)
      raise "#{dir} already exists, but is not a directory" unless File.directory?(dir)
      Dir.chdir(dir)
    end
    # puts "Dir is #{Dir.pwd}"
    # puts "File is #{filename}"
    f = File.open(filename, 'w+') { |file| file << output }
    File.chmod(0754, File.expand_path(filename)) if executable # TODO: this is unix-specific, should check os maybe?
    Dir.chdir(starting_directory)
    f
  end
end