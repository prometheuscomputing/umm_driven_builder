# must not have the word m-o-d-u-l-e above the next line (so that a Regexp can figure out the m-o-d-u-l-e name)
module UmlMetamodelDrivenBuilder
  
  # For more information about meta_info.rb, please see project MM, lib/mm/meta_info.rb
  
  # Required Strings
  GEM_NAME = "umm_driven_builder"
  VERSION  = '0.0.1'
  SUMMARY  = %q{Build things from instances of UmlMetamodel}
    
  # Optional String
  DESCRIPTION = %q{Build things from instances of UmlMetamodel.}
  HOMEPAGE    = nil

  # Optional Strings or Arrays of Strings
  AUTHORS = ["Michael Faughn"]
  EMAILS  = ["m.faughn@prometheuscomputing.com"]
  
  LANGUAGE = :ruby # the language the project is written in
  # This differs from Runtime version - this specifies the version of the syntax of LANGUAGE
  LANGUAGE_VERSION = ['>= 2.3']
  RUNTIME_VERSIONS = {
    :ruby  => ['>= 2.3']
  }

  # Only a library so these are without values
  APP_TYPES = []
  LAUNCHER  = nil
  
  # Optional Hashes - if nil, presumed to be empty.
  # There may be additional dependencies, for platforms (such as :maglev) other than :mri and :jruby
  # If JRuby platform Ruby code depends on a third party Java jar, that goes in DEPENDENCIES_JAVA
  DEPENDENCIES_RUBY = {
    :appellation        => '~> 0.0',
    :uml_metamodel      => '~> 3.0'
  }
  DEPENDENCIES_MRI   = { }
  DEPENDENCIES_JRUBY = { }
  DEVELOPMENT_DEPENDENCIES_RUBY  = { } 
  DEVELOPMENT_DEPENDENCIES_MRI   = { }
  DEVELOPMENT_DEPENDENCIES_JRUBY = { }
  
  # Java dependencies are harder to handle because Java does not have an equivalent of Ruby gem servers.
  # The closest thing is Maven repositories, which are growing in popularity, but not yet ubiquitous enough to warrant supporting in this tool.
  # Currently only the keys are used, version info is ignored.
  # Keys can be the names of Jars (complete, including any version info embedded in name) that must be located in JARCHIVE (key must end in .jar), or the name of a constant (key must not end in .jar).
  # Constsants must be defined in this module. Constants must not be computed from absolute paths (use environmental variables if necessary).
  # Support for constants is provided primarily to accomodate MagicDraw, which requires a large number of jars.
  DEPENDENCIES_JAVA    = { }
  DEPENDENCIES_JAVA_SE = { }
  DEPENDENCIES_JAVA_ME = { }
  DEPENDENCIES_JAVA_EE = { }
  
  # An Array of strings that YARD will interpret as regular expressions of files to be excluded.
  YARD_EXCLUDE = []
  
end